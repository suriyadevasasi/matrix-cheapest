﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;

namespace MatrixCheapest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public int rows;
        public int columns;
        public double[,] array;

        public Coordinate initialPoint;
        public Coordinate finalPoint;

        public string cheapestPath;
        public double cheapestScore;


        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnLoadMatrix_Click(object sender, RoutedEventArgs e)
        {
            //Open txt file for matrix
            System.Windows.Forms.OpenFileDialog theDialog = new System.Windows.Forms.OpenFileDialog();
            theDialog.Title = "Open Text File";
            theDialog.Filter = "TXT files|*.txt";
            theDialog.InitialDirectory = @"C:\";

            string rawtext = "";
            if (theDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    rawtext = File.ReadAllText(theDialog.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                    return;
                }
            }

            //string rawtext = "1.2 0.4 0.55 1.5; 3.1 2.82 1.6 8.1; 0.3 3.9 7.1 9.2";

            try
            {
                rows = rawtext.Split(';').Count();
                columns = rawtext.Split(';')[0].Split(' ').Count();

                array = new double[rows + 1, columns + 1];

                var allrows = rawtext.Split(';');

                int currentrow = 1;
                foreach (var row in allrows)
                {
                    int currentcolumn = 1;
                    foreach (var column in row.Split(' '))
                    {
                        if (column == "") continue;
                        array[currentrow, currentcolumn] = Convert.ToDouble(column);
                        currentcolumn++;
                    }
                    currentrow++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Error Matrix not found Original error: " + ex.Message);
                return;
            }



            //Show Data in Console
            textMatrix.Text = "";
            for (int i =1;i <= rows;i++)
            {
                
                string data = "";
                for (int j = 1; j <= columns; j++)
                {
                    data+= array[i, j] + "\t";
                }
                textMatrix.Text += data + Environment.NewLine;
                Console.WriteLine(data);
            }

            
        }

        private void btnFindPath_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                findPath();

                Console.WriteLine();
                Console.WriteLine("Cheapest Path: " + getDetailedPath(cheapestPath));
                Console.WriteLine("Cheapest Score: " + Math.Round(cheapestScore, 2));

                textPath.Text = getDetailedPath(cheapestPath);
                lblCost.Content = "Cost: " + Math.Round(cheapestScore, 2);

                visualizePath();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Error Occurred Original error: " + ex.Message);
                return;
            }

            
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            using (var sfd = new System.Windows.Forms.SaveFileDialog())
            {
                sfd.Filter = "txt files (*.txt)|*.txt";
                sfd.FilterIndex = 2;

                if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, getDetailedPath(cheapestPath));
                    MessageBox.Show("File saved as " + sfd.FileName);
                }
            }

        }

        public void findPath()
        {

            //array = new int[rows+1, columns+1];

            string path;

            try
            {
                initialPoint = new Coordinate(Convert.ToInt32(textInitialRow.Text), Convert.ToInt32(textInitialCol.Text));

                finalPoint = new Coordinate(Convert.ToInt32(textFinalRow.Text), Convert.ToInt32(textFinalCol.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Invalid Indexes Original error: " + ex.Message);
                return;
            }

            cheapestPath = "";
            cheapestScore = 9999999;


            path = String.Format("{0},{1}", initialPoint.x, initialPoint.y);

            recursive(path, initialPoint.x, initialPoint.y);

        }

        public void recursive(string path, int sx, int sy)
        {
            //array[sx,sy] = 1;
            if (sx == finalPoint.x && sy == finalPoint.y)
            {
                double total = 0;
                foreach(var data in path.Split('|'))
                {
                    var coords = data.Split(',');
                    Coordinate point = new Coordinate(Convert.ToInt32(coords[0]), Convert.ToInt32(coords[1]));

                    if (initialPoint.x == point.x && initialPoint.y == point.y) continue;
                    if (finalPoint.x == point.x && finalPoint.y == point.y) continue;
                    total += array[point.x, point.y];
                }

                if(total < cheapestScore)
                {
                    cheapestScore = total;
                    cheapestPath = path;
                }


                Console.WriteLine(path + " = " + total);
                return;
            }

            //right
            if (sy < columns)
            {
                if(!containsPoints(path ,sx, sy+1)) recursive(addToPath(path,sx, sy + 1),sx, sy + 1);

                //topright
                if(sx > 1) if (!containsPoints(path, sx - 1, sy + 1)) recursive(addToPath(path, sx - 1, sy + 1), sx - 1, sy + 1);

                //bottomright
                if(sx < rows) if (!containsPoints(path, sx + 1, sy + 1)) recursive(addToPath(path, sx + 1, sy + 1), sx + 1, sy + 1);
            }

            //left
            if (sy > 1)
            {
                if (!containsPoints(path, sx, sy -1)) recursive(addToPath(path, sx, sy - 1), sx, sy - 1);

                //topleft
                if (sx > 1) if (!containsPoints(path, sx - 1, sy - 1)) recursive(addToPath(path, sx - 1, sy - 1), sx - 1, sy - 1);

                //bottomleft
                if (sx < rows) if (!containsPoints(path, sx + 1, sy - 1)) recursive(addToPath(path, sx + 1, sy  - 1), sx + 1, sy - 1);
            }

            //up
            if (sx > 1 )
            {
                if (!containsPoints(path, sx-1, sy)) recursive(addToPath(path, sx - 1, sy), sx - 1, sy);
            }

            //down
            if (sx < rows)
            {

                if (!containsPoints(path, sx+1, sy)) recursive(addToPath(path, sx + 1, sy), sx + 1, sy);
            }

        }

        public string addToPath(string path, int x, int y)
        {
            path += String.Format("|{0},{1}", x,y);
            return path;
        }

        public bool containsPoints(string path, int x,int y)
        {
            return path.Contains(String.Format("{0},{1}", x, y));
        }

        public string getDetailedPath(string path)
        {
            string detailedPath = "";
            foreach(var data in path.Split('|'))
            {
                var coords = data.Split(',');
                Coordinate point = new Coordinate(Convert.ToInt32(coords[0]), Convert.ToInt32(coords[1]));

                detailedPath += String.Format("[{0},{1}]({2}) -> ", point.x, point.y, array[Convert.ToInt32(coords[0]), Convert.ToInt32(coords[1])]);
            }

            return detailedPath.Substring(0, detailedPath.Length - 4);
        }

        public void visualizePath()
        {
            textMatrix.Text = "";
            for (int i = 1; i <= rows; i++)
            {

                string datax = "";
                for (int j = 1; j <= columns; j++)
                {
                    datax += array[i, j] + "\t";
                }
                textMatrix.Text += datax + Environment.NewLine;
                Console.WriteLine(datax);
            }


            List<Coordinate> allpoints = new List<Coordinate>();

            foreach (var point in cheapestPath.Split('|'))
            {
                var coords = point.Split(',');
                allpoints.Add(new Coordinate(Convert.ToInt32(coords[0]), Convert.ToInt32(coords[1])));
            }

            string data = "";

            for (int i = 1; i <= rows; i++)
            {
                
                for (int j = 1; j <= columns; j++)
                {
                    if(allpoints.Where(point=>point.x == i && point.y == j).FirstOrDefault() == null) data += "-" + "\t";
                    else data += "+" + "\t";
                }
                data += Environment.NewLine;
                Console.WriteLine(data);
            }

            textMatrix.Text += Environment.NewLine + "Path Visualization" + Environment.NewLine + data;
        }

        
    }

    public class Coordinate
    {
        public int x;
        public int y;

        public Coordinate(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public string getFormattedPoint()
        {
            return String.Format("({0},{1})", x, y);
        }
    }
}
